import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:async';
import 'dart:math';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Beacon Grid',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Beacon Grid'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  List<double> currentConvertedPosition = [0,0];
  Position currentGPSPosition;

  Position beaconPosition;
  double setAngle = 0.0;  //TODO: make this changeable based on 2 pt selection,  shoudl be in radians
  List<List<double>>  currentPath = [];
  List<List<List<double>>>  paths=[];

  bool recordPath = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _startLocStream();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),

        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.gps_fixed),
              onPressed:() {_setCenter();
              }),
          IconButton(
              icon: Icon(Icons.looks_two),
              onPressed:() {_setSecondPt();
              }),
        ],
      ),
      body: Column(
        children: [

          Expanded(
            child: Container(
              padding: EdgeInsets.all(5.0),
              child: CustomPaint(
                painter: GridPainter(currentConvertedPosition[0],currentConvertedPosition[1],paths),
                child: Center(),
              ),
            ),
          ),
          Row(
            children: [
              RaisedButton(
                  child: Text("startPath"),
                  onPressed: () {
                    startPath();
                  }
              ),
              RaisedButton(
                  child: Text("stopPath"),
                  onPressed: () {
                    stopPath();
                  }
              ),
            ],
          ),
        ],
      ),

    );
  }

  //sets location at the 0 point of the grid;
  _setCenter() {
      beaconPosition = currentGPSPosition;
      print("updateBeaconPos to ${beaconPosition}");
  }

  _setSecondPt() {
    setAngle = calculateBearingAngle(beaconPosition, currentGPSPosition);
    print("setang =${setAngle}");
  }


  var geolocator = Geolocator();
  var locationOptions = LocationOptions(accuracy: LocationAccuracy.best, distanceFilter: 0);

_startLocStream() {
  StreamSubscription<Position> positionStream = geolocator.getPositionStream(locationOptions).listen(
          (Position position) {
        print(position == null ? 'Unknown' : position.latitude.toString() + ', ' + position.longitude.toString());
        currentGPSPosition = position;
        if (beaconPosition!= null) {
          updateCurrentPosition(position);

        }
      });
}

//converts GPS loc into a x,y coord (in m )
void updateCurrentPosition(Position currentGPSPos) {
  currentGPSPosition = currentGPSPos;
  setState(() {
    currentConvertedPosition = convertGPSposToRelativeLoc(currentGPSPosition);
    print("newPos = ${currentConvertedPosition}");
    if (recordPath) {
      currentPath.add(currentConvertedPosition);
      print("currentPathLen = ${currentPath.length}");
    }
  });

}

List<double> convertGPSposToRelativeLoc (Position toconvert) {
  final linearDist = calculateDistance(beaconPosition.latitude, beaconPosition.longitude, toconvert.latitude, toconvert.longitude);
  final bearingAngle = calculateBearingAngle(beaconPosition, toconvert);
  print("bearing angle = ${bearingAngle}");
  final adjBearingAngle = setAngle - bearingAngle;

  final xpos = linearDist*cos(adjBearingAngle);
  final ypos = -linearDist*sin(adjBearingAngle);
  return [xpos,ypos];
}

  /// Calculates the distance between two lat/long coordinates in m
  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return (12742000 * asin(sqrt(a))).abs();
  }

/*
//Source
  JSONObject source = step.getJSONObject("start_location");
  double lat1 = Double.parseDouble(source.getString("lat"));
  double lng1 = Double.parseDouble(source.getString("lng"));

// destination
  JSONObject destination = step.getJSONObject("end_location");
  double lat2 = Double.parseDouble(destination.getString("lat"));
  double lng2 = Double.parseDouble(destination.getString("lng"));

  double dLon = (lng2-lng1);
  double y = Math.sin(dLon) * Math.cos(lat2);
  double x = Math.cos(lat1)*Math.sin(lat2) - Math.sin(lat1)*Math.cos(lat2)*Math.cos(dLon);
  double brng = Math.toDegrees((Math.atan2(y, x)));
  brng = (360 - ((brng + 360) % 360));
  */

//gets bearing angle (in rad)
double calculateBearingAngle(Position beaconLoc, Position secondLoc) {
final dLon = secondLoc.longitude-beaconLoc.longitude;
final y = sin(dLon) * cos(secondLoc.latitude);
final x = cos(beaconLoc.latitude)*sin(secondLoc.latitude)- sin(beaconLoc.latitude)*cos(secondLoc.latitude)*cos(dLon);
final brng = atan2(y, x);
return brng;
}

void startPath() {
  recordPath = true;
  if (currentPath.length <1) {
    paths.add(currentPath);
    print("added path");
  }

  print("num Paths = ${paths.length}");
}

void stopPath() {
  if (currentPath.length > 0) {
    //paths.add(currentPath);
    currentPath = [];
    recordPath = false;
  }
}



}


//paints grid on map
class GridPainter extends CustomPainter {
double xLoc; //rel pos of user, m
double yLoc; //rel pos of user, m
List<List<List<double>>> paths;


GridPainter(this.xLoc,this.yLoc, this.paths);

final ySpacings = [-20,-15,-10,-5,0,5,10,15,20,25,30,35,40,45,50]; //m
final xSpacings = [0,5,10,15,20,25,30,35,40,45,50];//m

  @override
  void paint(Canvas canvas, Size size) {
    final height = size.height;
    final width = size.width;
    List<Color> colorList = [Colors.red,Colors.yellow,Colors.blue,Colors.green,Colors.purple,Colors.orange,Colors.brown,Colors.pink,Colors.cyan,Colors.indigo];

    //sets conversion to min of either height or width convert
    var conversion = width/(xSpacings.last - xSpacings.first); // in px/m
    final heightConvert = height/(ySpacings.last - ySpacings.first);
    if (heightConvert < conversion) {
      conversion = heightConvert;
    }

    // paints
    var axisLinePaint = Paint();
    axisLinePaint.strokeWidth = 2;
    axisLinePaint.style = PaintingStyle.stroke;
    axisLinePaint.color = Colors.grey[400];

    //draw Y lines
    for (final i in ySpacings) {
      if (i == 0) {
        axisLinePaint.strokeWidth = 4;
        axisLinePaint.color = Colors.grey[500];

      }
      canvas.drawLine(Offset(0, (i-ySpacings.first)*conversion),
          Offset(width,  (i-ySpacings.first)*conversion), axisLinePaint);
      axisLinePaint.strokeWidth = 2;
      axisLinePaint.color = Colors.grey[400];

    }


    //draw X lines
    for (final i in xSpacings) {
      canvas.drawLine(Offset(i*conversion,0),
          Offset(i*conversion,height), axisLinePaint);
    }

    //paths
    var pathPaint = Paint();
    pathPaint.strokeWidth = 2.0;
    pathPaint.style = PaintingStyle.stroke;
    pathPaint.color = Colors.purple;//RandomHexColor().colorRandom();

    if (paths.length > 0) {
      var counter = 0;
      for (final path in paths) {
        if (counter > 9 ) {
          counter = 0;
        }
        if (path.length >0) {
          pathPaint.color = colorList[counter];
          var pathToDraw = Path();
          pathToDraw.moveTo(path.first[0]*conversion, (path.first[1] - ySpacings.first)*conversion);
          for (final pt in path) {
              pathToDraw.lineTo(pt.first*conversion, (pt.last - ySpacings.first)*conversion);
          }
          canvas.drawPath(pathToDraw , pathPaint);
        }
        counter +=1;
      }
    }
    //draw beacon Marker
    final beaconMarkPaint = Paint();
    beaconMarkPaint.color = Colors.yellow;
    beaconMarkPaint.style = PaintingStyle.fill;
    canvas.drawCircle(Offset(0,-ySpacings.first*conversion), 10.0, beaconMarkPaint);

    //draw Location Marker
    final userMarkPaint = Paint();
    userMarkPaint.color = Colors.blue;
    userMarkPaint.style = PaintingStyle.fill;
    canvas.drawCircle(Offset(xLoc*conversion,(yLoc - ySpacings.first)*conversion), 10.0, userMarkPaint);

    
  }
  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}


class RandomHexColor {
  static const Color one = Colors.red;
  static const Color two = Colors.blue;
  static const Color three = Colors.yellow;

  List<Color> hexColor = [one, two, three];

  static final _random = Random();

  Color colorRandom() {
    return Color.fromRGBO(_random.nextInt(255), _random.nextInt(255), _random.nextInt(255), 1.0);
    //return hexColor[_random.nextInt(3)];
  }

}